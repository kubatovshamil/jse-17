package ru.t1.kubatov.tm.api.repository;

import ru.t1.kubatov.tm.enumerated.Sort;
import ru.t1.kubatov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void deleteAll(String userId);

    boolean existsById(String userId, String id);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    List<M> findAll(String userId, Sort sort);

    M findByID(String userId, String id);

    M findByIndex(String userId, Integer index);

    int getSize(String userId);

    M deleteByID(String userId, String id);

    M deleteByIndex(String userId, Integer index);

    M add(String userId, M model);

    M delete(String userId, M model);

}
