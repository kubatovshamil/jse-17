package ru.t1.kubatov.tm.command.task;

import ru.t1.kubatov.tm.enumerated.Sort;
import ru.t1.kubatov.tm.model.Task;
import ru.t1.kubatov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskListCommand extends AbstractTaskCommand {

    public final static String DESCRIPTION = "Show all Tasks.";

    public final static String NAME = "task-list";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Task> tasks = getTaskService().findAll(getUserId(), sort);
        showTaskList(tasks);
        System.out.println("[END]");
    }
}
