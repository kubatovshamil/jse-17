package ru.t1.kubatov.tm.command.task;

import ru.t1.kubatov.tm.model.Task;
import ru.t1.kubatov.tm.util.TerminalUtil;

public class TaskShowByIdCommand extends AbstractTaskCommand {

    public final static String DESCRIPTION = "Show Task by ID.";

    public final static String NAME = "task-show-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().findByID(getUserId(), id);
        showTask(task);
    }
}
