package ru.t1.kubatov.tm.exception.user;

import ru.t1.kubatov.tm.exception.system.AbstractSystemException;

public class AccessDeniedException extends AbstractSystemException {

    public AccessDeniedException() {
        super("Error! Wrong Login or Password...");
    }

}
