package ru.t1.kubatov.tm.exception.user;

import ru.t1.kubatov.tm.exception.system.AbstractSystemException;

public class UserNotLoggedInException extends AbstractSystemException {

    public UserNotLoggedInException() {
        super("Error! You haven't yet logged in...");
    }

}
