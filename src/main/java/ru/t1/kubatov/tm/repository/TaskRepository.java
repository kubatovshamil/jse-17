package ru.t1.kubatov.tm.repository;

import ru.t1.kubatov.tm.api.repository.ITaskRepository;
import ru.t1.kubatov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task add(final String userId, final String name, final String description) {
        final Task task = new Task(name, description);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public Task add(final String userId, final String name) {
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        return add(task);
    }

    public List<Task> findAllByProjectID(final String userId, final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : models) {
            if (task.getProjectId() == null) continue;
            if (!task.getProjectId().equals(projectId)) continue;
            if (!task.getUserId().equals(userId)) continue;
            result.add(task);
        }
        return result;
    }

    public List<Task> findAllByProjectID(final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : models) {
            if (task.getProjectId() == null) continue;
            if (!task.getProjectId().equals(projectId)) continue;
            result.add(task);
        }
        return result;
    }

}
